---
title: "Engineering Career Framework: Staff"
---

## Engineering Function Competencies: Staff
 
**Staff at GitLab are expected to exhibit the following competencies:**

- [Staff Leadership Competencies](#staff-leadership-competencies)
- [Staff Technical Competencies](#staff-technical-competencies)
- [Staff Values Alignment](#staff-values-alignment)
- [All competencies exhibited by a Senior](/handbook/engineering/career-development/matrix/engineering/senior/)
 
---

### Staff Leadership Competencies

{{% include "includes/engineering/staff-leadership-competency.md" %}}
  
### Staff Technical Competencies

{{% include "includes/engineering/staff-technical-competency.md" %}}

### Staff Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
